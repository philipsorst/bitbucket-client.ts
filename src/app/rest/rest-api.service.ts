import {HttpClient, HttpParams} from '@angular/common/http';
import {PaginatedResult} from './paginated-result';
import {Injectable} from '@angular/core';
import {EMPTY, Observable, ReplaySubject} from 'rxjs';
import {Issue} from '../issue/issue';
import {LocalstorageCache} from '../cache/localstorage-cache';
import {CacheInterface} from '../cache/cache-interface';
import {CacheEntry} from '../cache/cache-entry';
import {expand, map, reduce, tap} from 'rxjs/operators';
import {CacheOptions} from './cache-options';

@Injectable({
    providedIn: 'root'
})
export class RestApiService
{
    public static readonly BASE_URL = 'https://api.bitbucket.org/2.0/';

    private cache: CacheInterface;

    constructor(private httpClient: HttpClient)
    {
        this.cache = new LocalstorageCache('ddr_bbc.rest.', 60 * 60);
    }

    public list<T>(
        path: string,
        params?: HttpParams | { [param: string]: string | string[]; }
    ): Observable<PaginatedResult<T>>
    {
        return this.listUrl<T>(RestApiService.BASE_URL + path, params);
    }

    public listUrl<T>(
        url: string,
        params?: HttpParams | { [param: string]: string | string[]; }
    ): Observable<PaginatedResult<T>>
    {
        return this.httpClient.get<PaginatedResult<T>>(url, {params: params});
    }

    public listAllUrl<T>(
        firstUrl: string,
        params?: HttpParams | { [param: string]: string | string[]; },
        cacheOptions: CacheOptions = {cache: true, refresh: false}
    ): Observable<T[]>
    {
        const subject = new ReplaySubject<T[]>();

        let cacheKey = this.getCacheKey(firstUrl);
        if (true === cacheOptions.cache) {
            const cacheEntry = this.cache.getEntry<T[]>(cacheKey);
            if (null != cacheEntry) {
                subject.next(cacheEntry.value);
                if (false === cacheOptions.refresh && !CacheEntry.isExpired(cacheEntry) && !cacheEntry.stale) {
                    subject.complete();
                    return subject;
                }
            }
        }

        this.listUrl<T>(firstUrl, params).pipe(
            expand((paginated) => paginated.next ? this.listUrl<T>(paginated.next, params) : EMPTY),
            map((paginated) => paginated.values),
            reduce((acc, value) => acc.concat(value), []),
            tap((paginated) => {
                if (true === cacheOptions.cache) {
                    this.cache.set(cacheKey, paginated)
                }
            })
        ).subscribe(
            (paginated) => {
                subject.next(paginated);
                subject.complete();
            },
            (error) => {
                subject.error(error);
                subject.complete();
            }
        );

        return subject;
    }

    public get<T>(
        path: string, params?: HttpParams | { [param: string]: string | string[]; },
        cacheOptions: CacheOptions = {cache: true, refresh: true}
    ): Observable<T>
    {
        const subject = new ReplaySubject<T>();

        let url = RestApiService.BASE_URL + path;

        let cacheKey = this.getCacheKey(url);
        if (true === cacheOptions.cache) {
            const cacheEntry = this.cache.getEntry<T>(cacheKey);
            if (null != cacheEntry) {
                subject.next(cacheEntry.value);
                if (false === cacheOptions.refresh && !CacheEntry.isExpired(cacheEntry) && !cacheEntry.stale) {
                    subject.complete();
                    return subject;
                }
            }
        }

        this.httpClient.get<T>(url, {params: params}).subscribe(
            (result) => {
                if (true === cacheOptions.cache) {
                    this.cache.set<T>(cacheKey, result);
                }
                subject.next(result);
                subject.complete();
            },
            (error) => {
                subject.error(error);
                subject.complete();
            }
        );

        return subject;
    }

    public post<T>(path: string, issue: Issue): Observable<T>
    {
        return this.httpClient.post<T>(RestApiService.BASE_URL + path, issue);
    }

    public put<T>(path: string, issue: Issue): Observable<T>
    {
        let url = RestApiService.BASE_URL + path;
        let cacheKey = this.getCacheKey(url);
        return this.httpClient.put<T>(url, issue).pipe(
            tap((result) => {
                this.cache.set(cacheKey, result);
            })
        );
    }

    public delete<T>(path: string): Observable<any>
    {
        return this.httpClient.delete(RestApiService.BASE_URL + path);
    }

    public markStale(pathOrUrl: string)
    {
        this.cache.setStale(this.getCacheKey(pathOrUrl));
    }

    private getCacheKey(pathOrUrl: string): string
    {
        if (pathOrUrl.startsWith(RestApiService.BASE_URL)) {
            return pathOrUrl.substr(RestApiService.BASE_URL.length);
        } else {
            return pathOrUrl;
        }
    }
}

export class CacheOptions
{
    cache?: boolean = true;
    refresh?: boolean = false;
}

export class PaginatedResult<T>
{
    size: number;
    page: number;
    pagelen: number;
    next: string;
    previous: string;
    values: Array<T>;
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {IndexComponent} from './index/index.component';
import {AuthorizeComponent} from './security/authorize.component';
import {AccessTokenInterceptor} from './security/access-token-interceptor.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RepositoryComponent} from './repository/repository.component';
import {RepositoryListComponent} from './repository/repository-list.component';
import {RepositoryIndexComponent} from './repository/repository-index.component';
import {ExampleComponent} from './example/example.component';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar';
import {SidenavToggleComponent} from './sidenav/sidenav-toggle.component';
import {IssueListComponent} from './repository/issue/issue-list.component';
import {IssueEditComponent} from './repository/issue/issue-edit.component';
import {IssueDetailComponent} from './repository/issue/issue-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TextFieldModule} from '@angular/cdk/text-field';
import {BackArrowComponent} from './navigation/back-arrow.component';
import {ConfirmationDialogComponent} from './confirmation/confirmation-dialog.component';
import {RawHtmlPipe} from './common/raw-html.pipe';
import {IssueResolveDialogComponent} from './repository/issue/issue-resolve-dialog.component';
import {NgProgressModule} from '@ngx-progressbar/core';
import {NgProgressHttpModule} from '@ngx-progressbar/http';
import {FilterComponent} from './filter/filter.component';

@NgModule({
    declarations: [
        AppComponent,
        IndexComponent,
        AuthorizeComponent,
        RepositoryComponent,
        RepositoryListComponent,
        RepositoryIndexComponent,
        IssueListComponent,
        IssueDetailComponent,
        IssueEditComponent,
        ExampleComponent,
        SidenavToggleComponent,
        BackArrowComponent,
        ConfirmationDialogComponent,
        IssueResolveDialogComponent,
        RawHtmlPipe,
        FilterComponent
    ],
    entryComponents: [
        ConfirmationDialogComponent,
        IssueResolveDialogComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatListModule,
        MatSidenavModule,
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        MatCardModule,
        MatMenuModule,
        MatSnackBarModule,
        MatInputModule,
        MatFormFieldModule,
        MatDialogModule,
        TextFieldModule,
        NgProgressModule.withConfig({
            spinner: false,
            color: '#1976d2'
        }),
        NgProgressHttpModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        MatSelectModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AccessTokenInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule
{
}

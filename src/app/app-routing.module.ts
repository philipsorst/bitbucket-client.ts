import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthorizeComponent} from './security/authorize.component';
import {RepositoryListComponent} from './repository/repository-list.component';
import {RepositoryComponent} from './repository/repository.component';
import {RepositoryIndexComponent} from './repository/repository-index.component';
import {LoggedinAuthguardService} from './security/loggedin-authguard.service';
import {ExampleComponent} from './example/example.component';
import {IssueListComponent} from './repository/issue/issue-list.component';
import {IssueEditComponent} from './repository/issue/issue-edit.component';
import {IssueDetailComponent} from './repository/issue/issue-detail.component';
import {RepositoryResolver} from './repository/repository-resolver.service';
import {IndexComponent} from './index/index.component';
import {UserResolver} from './repository/user-resolver.service';

const routes: Routes = [
    {
        path: '',
        component: IndexComponent,
        canActivate: [LoggedinAuthguardService],
    },
    {
        path: 'authorize',
        component: AuthorizeComponent
    },
    {
        path: 'example',
        component: ExampleComponent
    },
    {
        path: 'repositories',
        canActivate: [LoggedinAuthguardService],
        children: [
            {
                path: ':username',
                resolve: {user: UserResolver},
                children: [
                    {
                        path: '',
                        component: RepositoryListComponent,
                    },
                    {
                        component: RepositoryComponent,
                        path: ':repo_slug',
                        resolve: {repository: RepositoryResolver},
                        children: [
                            {
                                path: 'issues',
                                component: IssueListComponent
                            },
                            {
                                path: 'issues/:issue_id',
                                component: IssueDetailComponent
                            },
                            {
                                path: 'issues/:issue_id/edit',
                                component: IssueEditComponent
                            },
                            {
                                path: '',
                                component: RepositoryIndexComponent
                            }
                        ]
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule
{
}

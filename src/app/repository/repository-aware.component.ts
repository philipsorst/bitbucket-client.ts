import {OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {RepositoryContext} from './repository-context.service';
import {Repository} from './repository';
import {User} from '../user/user';
import {tap} from 'rxjs/operators';

export class RepositoryAwareComponent implements OnInit, OnDestroy
{
    public user: User;

    public repository$: Observable<Repository>;

    public repository: Repository;

    private userSubscription: Subscription;

    constructor(protected repositoryContext: RepositoryContext)
    {
    }

    /**
     * @override
     */
    public ngOnInit(): void
    {
        this.userSubscription = this.repositoryContext.getCurrentUserObservable().subscribe((user) => {
            this.user = user;
            this.onUser(user);
        });

        this.repository$ = this.repositoryContext.getCurrentRepositoryObservable().pipe(
            tap(repository => {
                this.repository = repository;
                this.onRepository(repository);
            })
        );
    }

    /**
     * @override
     */
    public ngOnDestroy(): void
    {
        this.userSubscription.unsubscribe();
    }

    protected onRepository(repository: Repository)
    {
        /* Hook */
    }

    protected onUser(user: User)
    {
        /* Hook */
    }
}

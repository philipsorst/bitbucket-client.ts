import {Injectable} from '@angular/core';
import {Repository} from './repository';
import {Observable} from 'rxjs';
import {RestApiService} from '../rest/rest-api.service';
import {User} from '../user/user';
import {map} from 'rxjs/operators';
import {Issue} from '../issue/issue';
import {HttpParams} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RepositoryService
{
    constructor(private restApiService: RestApiService)
    {
    }

    public listAllByUser(user: User): Observable<Repository[]>
    {
        return this.restApiService.listAllUrl<Repository>(user.links.repositories.href);
    }

    public get(username: string, repo_slug: string): Observable<Repository>
    {
        return this.restApiService.get<Repository>('repositories/' + username + '/' + repo_slug);
    }

    public getIssue(repository: Repository, issue_id: number): Observable<Issue>
    {
        return this.restApiService.get<Issue>(this.getIssuePath(repository, issue_id));
    }

    private getIssuePath(repository: Repository, issue_id: number)
    {
        return 'repositories/' + repository.owner.username + '/' + repository.slug + '/issues/' + issue_id;
    }

    public listAllIssues(repository: Repository): Observable<Issue[]>
    {
        const params = new HttpParams().set('q', 'state="new" OR state="open"');

        return this.restApiService.listAllUrl<Issue>(repository.links.issues.href, params);
    }

    public deleteIssue(repository: Repository, issue: Issue)
    {
        return this.restApiService.delete('repositories/' + repository.owner.username + '/' + repository.name + '/issues/' + issue.id).pipe(
            map(() => {
                this.restApiService.markStale(repository.links.issues.href);
            })
        );
    }
}

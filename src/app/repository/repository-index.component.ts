import {Component} from '@angular/core';
import {RepositoryAwareComponent} from './repository-aware.component';
import {RepositoryContext} from './repository-context.service';

@Component({
    templateUrl: './repository-index.component.html'
})
export class RepositoryIndexComponent extends RepositoryAwareComponent
{
    constructor(protected repositoryContext: RepositoryContext)
    {
        super(repositoryContext);
    }
}

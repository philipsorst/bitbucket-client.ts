import {Component} from '@angular/core';
import {RepositoryContext} from '../repository-context.service';
import {RepositoryService} from '../repository.service';
import {RepositoryAwareComponent} from '../repository-aware.component';
import {Issue} from '../../issue/issue';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from '../../confirmation/confirmation-dialog.component';
import {IssueService} from '../../issue/issue.service';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

@Component({
    templateUrl: './issue-list.component.html'
})
export class IssueListComponent extends RepositoryAwareComponent
{
    private refreshSubject$ = new BehaviorSubject(undefined);

    public issues$: Observable<Issue[]>;

    public filter$ = new BehaviorSubject<string>(null);

    constructor(
        protected repositoryContext: RepositoryContext,
        private repositoryService: RepositoryService,
        protected route: ActivatedRoute,
        private issueService: IssueService,
        private dialog: MatDialog
    )
    {
        super(repositoryContext);
    }

    /**
     * @override
     */
    public ngOnInit(): void
    {
        super.ngOnInit();
        this.issues$ = combineLatest([this.repository$, this.filter$, this.refreshSubject$]).pipe(
            switchMap(([repository, filter, refresh]) => {
                return this.repositoryService.listAllIssues(repository).pipe(
                    map(issues => issues
                        .filter((issue) => {
                            if (null == filter || '' === filter.trim()) {
                                return true;
                            }

                            return -1 !== issue.title.toLocaleLowerCase().indexOf(filter.toLocaleLowerCase());
                        })
                    )
                )
            })
        );
    }

    public resolve(issue: Issue)
    {
        this.issueService.resolve(this.repository, issue).subscribe(() => {
            this.refreshSubject$.next(undefined);
        });
    }

    public deleteIssue(issue: Issue)
    {
        let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
            data: {
                'title': 'Delete Issue',
                'content': 'Are you sure you want to delete the Issue: ' + issue.title
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (true === result) {
                this.repositoryService.deleteIssue(this.repository, issue).subscribe(() => {
                    this.refreshSubject$.next(undefined);
                });
            }
        });
    }

    public refresh()
    {
        this.refreshSubject$.next(undefined);
    }

    public trackByFn(index: number, issue: Issue)
    {
        return issue.id;
    }

    public setFilter(filterString: string)
    {
        this.filter$.next(filterString);
    }
}

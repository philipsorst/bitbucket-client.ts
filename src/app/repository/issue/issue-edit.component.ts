import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RepositoryAwareComponent} from '../repository-aware.component';
import {RepositoryContext} from '../repository-context.service';
import {combineLatest, Observable, of} from 'rxjs';
import {RepositoryService} from '../repository.service';
import {Issue} from '../../issue/issue';
import {MatSnackBar} from '@angular/material/snack-bar';
import {IssueContent} from '../../issue/issue-content';
import {IssueService} from '../../issue/issue.service';
import {switchMap} from 'rxjs/operators';
import {Repository} from '../repository';

@Component({
    templateUrl: './issue-edit.component.html'
})
export class IssueEditComponent extends RepositoryAwareComponent
{
    public issue$: Observable<Issue>;

    public kinds = Issue.KINDS;

    public priorities = Issue.PRIORITIES;

    public states = Issue.STATES;

    public saving: boolean = false;

    constructor(
        protected repositoryContext: RepositoryContext,
        private repositoryService: RepositoryService,
        protected route: ActivatedRoute,
        private router: Router,
        private snackBar: MatSnackBar,
        private issueService: IssueService
    )
    {
        super(repositoryContext);
    }

    /**
     * @override
     */
    public ngOnInit(): void
    {
        super.ngOnInit();

        this.issue$ = combineLatest([this.repository$, this.route.params]).pipe(
            switchMap(([repository, params]) => this.load(repository, params.issue_id))
        );
    }

    /**
     * @override
     */
    public ngOnDestroy(): void
    {
        super.ngOnDestroy();
    }

    public save(issue: Issue)
    {
        this.saving = true;
        this.issueService.save(this.repository, issue).subscribe(
            (issue) => {
                this.router.navigate(['/repositories', this.repositoryContext.username, this.repositoryContext.repo_slug, 'issues']);
            },
            (error) => {
                this.snackBar.open('Could not save', 'OK');
            }
        ).add(() => this.saving = false);
    }

    private load(repository: Repository, issue_id: number | string): Observable<Issue>
    {
        if ('new' === issue_id) {
            const issue = new Issue();
            issue.content = new IssueContent();
            return of(issue)
        } else {
            return this.repositoryService.getIssue(repository, issue_id as number);
        }
    }
}

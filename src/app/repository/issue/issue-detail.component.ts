import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RepositoryAwareComponent} from '../repository-aware.component';
import {RepositoryContext} from '../repository-context.service';
import {combineLatest, Observable} from 'rxjs';
import {RepositoryService} from '../repository.service';
import {Issue} from '../../issue/issue';
import {switchMap} from 'rxjs/operators';

@Component({
    templateUrl: './issue-detail.component.html'
})
export class IssueDetailComponent extends RepositoryAwareComponent
{
    public issue$: Observable<Issue>;

    constructor(
        protected repositoryContext: RepositoryContext,
        private repositoryService: RepositoryService,
        protected route: ActivatedRoute
    )
    {
        super(repositoryContext);
    }

    /**
     * @override
     */
    public ngOnInit(): void
    {
        super.ngOnInit();

        this.issue$ = combineLatest([this.repository$, this.route.params]).pipe(
            switchMap(([repository, params]) => this.repositoryService.getIssue(repository, params.issue_id))
        );
    }
}

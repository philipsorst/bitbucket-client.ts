import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Issue} from '../../issue/issue';

@Component({
    templateUrl: './issue-resolve-dialog.component.html'
})
export class IssueResolveDialogComponent
{
    public issue: Issue;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any)
    {
        this.issue = data.issue;
    }
}

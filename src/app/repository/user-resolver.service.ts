import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {User} from '../user/user';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from '../user/user.service';
import {RepositoryContext} from './repository-context.service';
import {tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserResolver implements Resolve<User>
{
    constructor(private userService: UserService, private repositoryContext: RepositoryContext)
    {
    }

    /**
     * @override
     */
    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | User
    {
        return this.userService.get(route.params['username']).pipe(
            tap((user) => {
                this.repositoryContext.setCurrentUser(user);
            })
        );
    }
}

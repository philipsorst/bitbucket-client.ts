import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Repository} from './repository';
import {RepositoryContext} from './repository-context.service';
import {RepositoryService} from './repository.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class RepositoryResolver implements Resolve<Repository>
{
    constructor(private repositoryService: RepositoryService, private repositoryContext: RepositoryContext)
    {
    }

    /**
     * @override
     */
    public resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<Repository> | Promise<Repository> | Repository
    {
        this.repositoryContext.username = route.params.username;
        this.repositoryContext.repo_slug = route.params.repo_slug;

        return this.repositoryService.get(route.params.username, route.params.repo_slug).pipe(
            map((repository) => {
                repository.owner.username = route.params.username;
                this.repositoryContext.setCurrentRepository(repository);
                return repository;
            })
        );
    }
}

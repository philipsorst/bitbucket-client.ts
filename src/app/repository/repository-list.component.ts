import {Component} from '@angular/core';
import {RepositoryService} from './repository.service';
import {SecurityService} from '../security/security.service';
import {Repository} from './repository';
import {ActivatedRoute} from '@angular/router';
import {User} from '../user/user';
import {RepositoryAwareComponent} from './repository-aware.component';
import {RepositoryContext} from './repository-context.service';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
    templateUrl: './repository-list.component.html'
})
export class RepositoryListComponent extends RepositoryAwareComponent
{
    public repositories$: Observable<Repository[]>;

    public filter$ = new BehaviorSubject<string>(null);

    constructor(
        protected repositoryContext: RepositoryContext,
        private repositoryService: RepositoryService,
        private securityService: SecurityService,
        private route: ActivatedRoute
    )
    {
        super(repositoryContext);
    }

    /**
     * @override
     */
    protected onUser(user: User)
    {
        this.repositories$ = combineLatest([this.repositoryService.listAllByUser(user), this.filter$]).pipe(
            map(([repositories, filter]) =>
                repositories
                    .sort((left, right) => right.updated_on.localeCompare(left.updated_on))
                    .filter(repository => {
                        if (null == filter || '' === filter.trim()) {
                            return true;
                        }

                        return -1 !== repository.slug.toLocaleLowerCase().indexOf(filter.toLocaleLowerCase());
                    })

            )
        );
    }

    public trackByFn(index: number, repository: Repository)
    {
        return repository.slug;
    }

    public setFilter(filterString: string)
    {
        this.filter$.next(filterString);
    }
}

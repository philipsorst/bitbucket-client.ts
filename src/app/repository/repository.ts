import {User} from '../user/user';
import {Link} from '../link/link';

export class Repository
{
    uuid: string;
    name: string;
    full_name: string;
    language: string;
    slug: string;
    created_on: string;
    updated_on: string;
    owner: User;
    links: {
        issues: Link;
    };
    repository: Repository;
}

import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RepositoryService} from './repository.service';
import {RepositoryContext} from './repository-context.service';
import {RepositoryAwareComponent} from './repository-aware.component';

@Component({
    templateUrl: './repository.component.html'
})
export class RepositoryComponent extends RepositoryAwareComponent
{
    constructor(
        protected route: ActivatedRoute,
        private repositoryService: RepositoryService,
        protected repositoryContext: RepositoryContext
    )
    {
        super(repositoryContext)
    }
}

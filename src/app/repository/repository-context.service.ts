import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Repository} from './repository';
import {User} from '../user/user';

@Injectable({
    providedIn: 'root'
})
export class RepositoryContext
{
    public username: string;

    public repo_slug: string;

    private currentRepository = new BehaviorSubject<Repository>(null);

    private currentUser = new BehaviorSubject<User>(null);

    public setCurrentRepository(repository: Repository)
    {
        this.currentRepository.next(repository);
    }

    public getCurrentRepository(): Repository | null
    {
        return this.currentRepository.getValue();
    }

    public getCurrentRepositoryObservable(): Observable<Repository>
    {
        return this.currentRepository.asObservable();
    }

    public setCurrentUser(user: User)
    {
        this.currentUser.next(user);
    }

    public getCurrentUser(): User | null
    {
        return this.currentUser.getValue();
    }

    public getCurrentUserObservable(): Observable<User>
    {
        return this.currentUser.asObservable();
    }
}

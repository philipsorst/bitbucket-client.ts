import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from './security.service';
import {HttpClient} from '@angular/common/http';
import {RestApiService} from '../rest/rest-api.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
    templateUrl: './authorize.component.html'
})
export class AuthorizeComponent implements OnInit
{
    constructor(
        private route: ActivatedRoute,
        private securityService: SecurityService,
        private httpClient: HttpClient,
        private restApiService: RestApiService,
        private router: Router,
        private snackBar: MatSnackBar
    )
    {
    }

    /**
     * @override
     */
    public ngOnInit(): void
    {
        let fragmentParams = new Map<string, string>();
        let parts = this.route.snapshot.fragment.split('&').forEach((part: string) => {
            let matches = part.match(/([^=]+)=(.+)/);
            if (null !== matches) {
                fragmentParams.set(matches[1], matches[2]);
            }
        });

        if (!fragmentParams.has('access_token')) {
            this.snackBar.open('No Access Token found', 'OK');
            return;
        }

        this.securityService.setAccessToken(fragmentParams.get('access_token'));
        this.securityService.login().subscribe(
            (user) => {
                this.router.navigateByUrl(localStorage.getItem(SecurityService.RETURN_URL_KEY));
            },
            (error) => {
                this.snackBar.open('Login failed', 'OK');
            }
        );
    }
}

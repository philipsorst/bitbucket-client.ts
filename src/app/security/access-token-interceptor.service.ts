import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {SecurityService} from './security.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class AccessTokenInterceptor implements HttpInterceptor
{
    constructor(private router: Router, private securityService: SecurityService, private route: ActivatedRoute)
    {
    }

    /**
     * @override
     */
    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        const accessToken = this.securityService.getAccessToken();

        if (null != accessToken) {
            req = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + accessToken)
            });
        }

        return next.handle(req).pipe(catchError((err) => {

            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    //TODO: Should use refresh token
                    this.securityService.clearAccessToken();
                    localStorage.setItem(SecurityService.RETURN_URL_KEY, this.router.url);
                    SecurityService.redirectToRemoteAuthorizeUrl();
                }
            }

            return throwError(err);
        }));
    }
}

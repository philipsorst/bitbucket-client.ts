import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {SecurityService} from './security.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoggedinAuthguardService implements CanActivate
{
    constructor(private router: Router, private securityService: SecurityService)
    {
    }

    /**
     * @override
     */
    public canActivate(
        route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean>
    {
        if (this.securityService.isLoggedIn()) {
            return true;
        }

        localStorage.setItem(SecurityService.RETURN_URL_KEY, state.url);
        return this.securityService.login().pipe(map((user) => {
            return true;
        }));
    }
}

import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../user/user';
import {RestApiService} from '../rest/rest-api.service';
import {environment} from '../../environments/environment';
import {tap} from 'rxjs/internal/operators/tap';

@Injectable({
    providedIn: 'root'
})
export class SecurityService
{
    public static readonly RETURN_URL_KEY = 'ddr_bbc.return_url';

    public static readonly ACCESS_TOKEN_KEY = 'ddr_bbr.access_token';

    public static readonly REMOTE_AUTHORIZE_URL = 'https://bitbucket.org/site/oauth2/authorize?client_id=' + environment.clientId + '&response_type=token';

    private currentUserSubject = new BehaviorSubject<User>(null);

    constructor(private restApiService: RestApiService)
    {
    }

    public getAccessToken()
    {
        return localStorage.getItem(SecurityService.ACCESS_TOKEN_KEY);
    }

    public setAccessToken(accessToken: string)
    {
        localStorage.setItem(SecurityService.ACCESS_TOKEN_KEY, accessToken);
    }

    public clearAccessToken()
    {
        localStorage.removeItem(SecurityService.ACCESS_TOKEN_KEY);
    }

    public getCurrentUser()
    {
        return this.currentUserSubject.getValue();
    }

    public getCurrentUserObservable(): Observable<User>
    {
        return this.currentUserSubject.asObservable();
    }

    public isLoggedIn(): boolean
    {
        return null != this.getCurrentUser();
    }

    public login(): Observable<User>
    {
        if (null != this.getAccessToken()) {
            return this.restApiService.get<User>('user', {}, {cache: false}).pipe(tap((user) => {
                this.currentUserSubject.next(user);
            }));
        }

        SecurityService.redirectToRemoteAuthorizeUrl();
    }

    public static redirectToRemoteAuthorizeUrl()
    {
        window.location.href = SecurityService.REMOTE_AUTHORIZE_URL;
    }
}

import {Injectable} from "@angular/core";
import {MatSidenav} from "@angular/material/sidenav";
import {MatDrawerToggleResult} from '@angular/material/sidenav/typings/drawer';

@Injectable({
    providedIn: 'root'
})
export class SidenavService
{
    private sidenav: MatSidenav;

    public setSidenav(sidenav: MatSidenav)
    {
        this.sidenav = sidenav;
    }

    public toggle(): Promise<MatDrawerToggleResult>
    {
        return this.sidenav.toggle();
    }
}

import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {SidenavService} from './sidenav/sidenav.service';
import {SecurityService} from './security/security.service';
import {User} from './user/user';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, OnDestroy
{
    @ViewChild('sidenav', {static: true}) public sidenav: MatSidenav;

    public user: User;

    private userSubscription: Subscription;

    constructor(
        private sidenavService: SidenavService,
        private securityService: SecurityService,
    )
    {
    }

    /**
     * @override
     */
    public ngOnInit(): void
    {
        this.sidenavService.setSidenav(this.sidenav);
        this.userSubscription = this.securityService.getCurrentUserObservable().subscribe((user) => {
            this.user = user;
        })
    }

    /**
     * @override
     */
    public ngOnDestroy(): void
    {
        this.userSubscription.unsubscribe();
    }
}

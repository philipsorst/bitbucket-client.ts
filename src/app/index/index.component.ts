import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../security/security.service';
import {Router} from '@angular/router';

@Component({
    template: 'Redirecting'
})
export class IndexComponent implements OnInit
{
    constructor(private securityService: SecurityService, private router: Router)
    {
    }

    /**
     * @override
     */
    public ngOnInit(): void
    {
        this.router.navigate(['/repositories', this.securityService.getCurrentUser().username]);
    }
}

import {Component} from '@angular/core';
import {empty, Observable} from 'rxjs';
import {of} from 'rxjs/internal/observable/of';
import {expand, map, reduce} from 'rxjs/operators';

@Component({
    template: 'Example'
})
export class ExampleComponent
{
    generate(next: number): Observable<ExamplePaginated<number>>
    {
        let values = [];
        let newNext = null;
        for (let i = 0; i < next; i++) {
            values.push(next);
        }

        if (next < 10) {
            newNext = next + 1;
        }

        return of(new ExamplePaginated<number>(values, newNext));
    }

    public ngOnInit()
    {
        this.generate(1).pipe(
            expand((value, index) => value.next ? this.generate(value.next) : empty()),
            map((value) => value.values),
            reduce((acc, value) => acc.concat(value), [])
        ).subscribe((result) => {
            console.log('New result', result);
        })
    }
}

class ExamplePaginated<T>
{
    next: number;
    values: Array<T>;

    constructor(values: Array<T>, next: number = null)
    {
        this.next = next;
        this.values = values;
    }
}

import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Pipe({name: 'rawHtml', pure: false})
export class RawHtmlPipe implements PipeTransform
{
    constructor(private sanitizer: DomSanitizer)
    {
    }

    /**
     * @override
     */
    public transform(content: string): SafeHtml
    {
        return this.sanitizer.bypassSecurityTrustHtml(content);
    }
}

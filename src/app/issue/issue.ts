import {User} from '../user/user';
import {IssueContent} from './issue-content';

export class Issue
{
    public static readonly STATES = [
        'new',
        'open',
        'resolved',
        'on hold',
        'invalid',
        'duplicate',
        'wontfix',
        'closed'
    ];

    public static readonly KINDS = [
        'bug',
        'enhancement',
        'proposal',
        'task'
    ];

    public static readonly PRIORITIES = [
        'trivial',
        'minor',
        'major',
        'critical',
        'blocker',
    ];

    id: number;
    assignee: User;
    title: string;
    priority: string = 'major';
    state: string = 'new';
    kind: string = 'bug';
    reporter: User;
    content: IssueContent;
    updated_on: string;
    created_on: string;
}

export class IssueContent
{
    raw: string = '';
    markup: string = 'markdown';
    html: string;
    type: string;
}

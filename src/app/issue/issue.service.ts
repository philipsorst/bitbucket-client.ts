import {Injectable} from '@angular/core';
import {Issue} from './issue';
import {Repository} from '../repository/repository';
import {MatDialog} from '@angular/material/dialog';
import {IssueResolveDialogComponent} from '../repository/issue/issue-resolve-dialog.component';
import {RestApiService} from '../rest/rest-api.service';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class IssueService
{
    constructor(private restApiService: RestApiService, private dialog: MatDialog)
    {
    }

    public resolve(repository: Repository, issue: Issue): Observable<Issue>
    {
        let observable = new Subject<Issue>();

        let dialogRef = this.dialog.open(IssueResolveDialogComponent, {data: {'issue': issue}});
        dialogRef.afterClosed().subscribe((result) => {
            if (true === result) {
                issue.state = 'resolved';
                this.save(repository, issue).subscribe(
                    (issue) => {
                        observable.next(issue);
                        observable.complete();
                    },
                    (error) => {
                        observable.error(error);
                        observable.complete()
                    });
            } else {
                observable.next(null);
                observable.complete();
            }
        });

        return observable;
    }

    public save(repository: Repository, issue: Issue): Observable<Issue>
    {
        if (null == issue.id) {
            return this.restApiService.post<Issue>('repositories/' + repository.owner.username + '/' + repository.name + '/issues', issue).pipe(map((issue) => {
                this.restApiService.markStale(repository.links.issues.href);
                return issue;
            }));
        } else {
            return this.restApiService.put<Issue>('repositories/' + repository.owner.username + '/' + repository.name + '/issues/' + issue.id, issue).pipe(map((issue) => {
                this.restApiService.markStale(repository.links.issues.href);
                return issue;
            }));
        }
    }
}

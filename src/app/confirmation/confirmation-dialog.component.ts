import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    templateUrl: './confirmation-dialog.component.html'
})
export class ConfirmationDialogComponent
{
    public title: string;
    public content: string;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any)
    {
        this.title = data.title;
        this.content = data.content;
    }
}

import {Component} from '@angular/core';
import {Location} from '@angular/common';

@Component({
    selector: 'ddr-back-arrow',
    template: `
        <button mat-icon-button (click)="back()">
            <mat-icon>arrow_back</mat-icon>
        </button>`
})
export class BackArrowComponent
{
    constructor(private location: Location)
    {
    }

    public back()
    {
        this.location.back();
    }
}

import {Link} from '../link/link';

export class User
{
    username: string;
    display_name: string;
    uuid: string;
    links: {
        avatar: Link;
        repositories: Link;
    }
}

import {Injectable} from '@angular/core';
import {RestApiService} from '../rest/rest-api.service';
import {Observable} from 'rxjs';
import {User} from './user';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserService
{
    constructor(private restApiService: RestApiService)
    {
    }

    public get(username: string): Observable<User>
    {
        return this.restApiService.get<User>('users/' + username)
            .pipe(map((user) => {
                user.username = username;
                return user;
            }));
    }
}

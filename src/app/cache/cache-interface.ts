import {CacheEntry} from './cache-entry';

export interface CacheInterface
{
    set<T>(key: string, value: T);

    set<T>(key: string, value: T, ttl: number);

    setEntry<T>(key: string, entry: CacheEntry<T>);

    has(key: string): boolean;

    get<T>(key: string): T | null;

    getEntry<T>(key: string): CacheEntry<T> | null

    setStale(key: string)
}

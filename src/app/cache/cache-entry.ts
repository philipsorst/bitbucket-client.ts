export class CacheEntry<T>
{
    expiry: number;
    value: T;
    stale: boolean = false;

    constructor(value: T, expiry: number)
    {
        this.expiry = expiry;
        this.value = value;
    }

    public static isExpired<T>(entry: CacheEntry<T>): boolean
    {
        return entry.expiry < Date.now();
    }
}

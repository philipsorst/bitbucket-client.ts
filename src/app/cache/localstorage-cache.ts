import {CacheInterface} from './cache-interface';
import {CacheEntry} from './cache-entry';

export class LocalstorageCache implements CacheInterface
{
    constructor(private prefix: string, private defaultTtl: number = 60)
    {
    }

    public get<T>(key: string): T | null
    {
        const entry = this.getEntry<T>(key);
        if (null == entry) {
            return null;
        }

        if (CacheEntry.isExpired(entry)) {
            localStorage.removeItem(this.prefix + key);
            return null;
        }

        return entry.value;
    }

    public getEntry<T>(key: string): CacheEntry<T> | null
    {
        const entryString = localStorage.getItem(this.prefix + key);
        if (null == entryString) {
            return null;
        }

        return JSON.parse(entryString) as CacheEntry<T>;
    }

    public has(key: string): boolean
    {
        const entryString = localStorage.getItem(this.prefix + key);
        if (null == entryString) {
            return false;
        }

        const entry = JSON.parse(entryString) as CacheEntry<any>;

        return !CacheEntry.isExpired(entry);
    }

    public set<T>(key: string, value: T, ttl: number = this.defaultTtl)
    {
        this.setEntry(key, new CacheEntry(value, Date.now() + ttl * 1000));
    }

    public setEntry<T>(key: string, entry: CacheEntry<T>)
    {
        localStorage.setItem(this.prefix + key, JSON.stringify(entry));
    }

    public setStale(key: string)
    {
        const entry = this.getEntry(key);
        if (null != entry) {
            entry.stale = true;
            this.setEntry(key, entry);
        }
    }
}
